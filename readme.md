# Lipa na Mpesa Online API

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
- GO v1.10.4
- `chi` Routing Package
- `dep` Dependancy Manager
- `MySQL` Database
- `dotenv` dotenv


##Installing
- Copy files to your local machine
- Navigate to the file path in your computer
- You also need to have the following GO packages installed

    ```
    go1.10.4
    ```

- Using the existing .env.example template, configure your environment variables and save the file as .env

## Launching the program
- Run a `go run *.go` command in your terminal and it will serve on port specified in the .env file

## API End Points

#### Create Merchant
    Path : /merchant/create
    Method: POST
    Fields: {
        business_type, 
        business_name, 
        industry, 
    }
    Response: 201

## Running the tests
Onging development and deployment tests

## Deployment
Not yet deployed to a live system

## Built With
- GoLang - The core backend language

## Authors
Kennedy Kinoti

## Acknowledgments
Once more, I give a thumbs up to google

> "One of the best programming skills you can have is knowing when to walk away for awhile."

💬 Oscar Godson