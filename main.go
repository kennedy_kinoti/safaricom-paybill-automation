package main

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

var router *chi.Mux
var db *sql.DB

// Details Struct
type Details struct {
	BusinessShortCode string `json:"short_code"`
	TransactionType   string `json:"transaction_type"`
	Amount            string `json:"amount"`
	PartyA            string `json:"party_A"`
	PartyB            string `json:"party_B"`
	PhoneNumber       string `json:"phone_number"`
	CallBackURL       string `json:"callback_URL"`
	AccountReference  string `json:"account_ref"`
	TransactionDesc   string `json:"transaction_ref"`
}

func init() {
	router = chi.NewRouter()

	// Basic CORS
	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	router.Use(middleware.Recoverer)
	router.Use(cors.Handler)

	godotenv.Load()
}

func routers() *chi.Mux {
	router.Get("/", ping)

	// Affiliates Dashboard API's
	router.Get("/lnm_online", LipaNaMpesaOnline)
	router.Get("/lnm_confirm", ConfirmPayment)

	return router
}

// server starting point
func ping(w http.ResponseWriter, r *http.Request) {
	respondwithJSON(w, http.StatusOK, map[string]string{"message": "Welcome to LNM API"})
}

// LipaNaMpesaOnline function
func LipaNaMpesaOnline(w http.ResponseWriter, r *http.Request) {

	details := Details{}
	json.NewDecoder(r.Body).Decode(&details)

	url := "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
	loc, _ := time.LoadLocation("Africa/Nairobi")
	t := time.Now().In(loc)
	time := t.Format("2006-01-02 15:04:05")

	myString := "This is golangcode.com testing base 64!"

	// Encode
	passwordEncoded := base64.StdEncoding.EncodeToString([]byte(myString))

	payload := strings.NewReader("{\r\n    \"BusinessShortCode\": \"" + details.BusinessShortCode + "\",\r\n    \"Password\": \"" + passwordEncoded + "\",\r\n    \"Timestamp\": \"" + time + "\",\r\n    \"TransactionType\": \"CustomerPayBillOnline\",\r\n    \"Amount\": \"" + details.Amount + "\",\r\n    \"PartyA\": \"" + details.PartyA + "\",\r\n    \"PartyB\": \"" + details.PartyB + "\",\r\n    \"PhoneNumber\": \"" + details.PhoneNumber + "\",\r\n    \"CallBackURL\": \"https://ip_address:port/callback\",\r\n    \"AccountReference\": \"" + details.AccountReference + "\",\r\n    \"TransactionDesc\": \"" + details.TransactionDesc + "\"\r\n}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("Authorization", "Bearer <Access-Token>")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("cache-control", "no-cache")
	req.Header.Add("Postman-Token", "8a799614-ea66-499c-ad5e-ba0445598fe5")

	res, _ := http.DefaultClient.Do(req)

	if res != nil {
		defer res.Body.Close()
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}

// ConfirmPayment sample
func ConfirmPayment(w http.ResponseWriter, r *http.Request){

}

func main() {

	routers()

	if err := godotenv.Load(); err != nil {
		fmt.Println("File .env not found, reading port configuration from ENV")
	}

	port := os.Getenv("port")

	fmt.Println(":" + port)

	http.ListenAndServe(":"+port, Logger())

}
